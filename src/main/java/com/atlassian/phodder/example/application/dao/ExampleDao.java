package com.atlassian.phodder.example.application.dao;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;

public interface ExampleDao {
    @SqlQuery("select name from something where id = :id")
    String findNameById(@Bind("id") int id);
}
